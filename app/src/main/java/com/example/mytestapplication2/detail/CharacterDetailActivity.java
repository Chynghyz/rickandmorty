package com.example.mytestapplication2.detail;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.example.mytestapplication2.R;
import com.example.mytestapplication2.model.Character;

public class CharacterDetailActivity extends AppCompatActivity {
    private Character character;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.character_detail);
        character = (Character) getIntent().getSerializableExtra("character");
        setCharacter();
    }

    private void setCharacter() {
        setTitle(character.getName());
        Glide.with(this).load(character.getImage()).into(((ImageView) findViewById(R.id.image)));
        ((TextView) findViewById(R.id.origin)).setText("From: " + character.getOrigin().getName());
        ((TextView) findViewById(R.id.location)).setText("Location: " + character.getLocation().getName());
        ((TextView) findViewById(R.id.gender)).setText("Gender: " + character.getGender());
        ((TextView) findViewById(R.id.status)).setText("Status: " + character.getStatus());
        ((TextView) findViewById(R.id.species)).setText("Species: " + character.getSpecies());
        ((TextView) findViewById(R.id.type)).setText("Type: " + character.getType());
        ((TextView) findViewById(R.id.episodes)).setText("Episodes count: " + character.getEpisode().size());
    }
}
