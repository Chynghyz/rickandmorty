package com.example.mytestapplication2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.mytestapplication2.model.Character;

import java.util.ArrayList;
import java.util.List;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder> {
    private List<Character> characterList = new ArrayList<>();
    private CharacterClickListener listener;

    public CharacterAdapter(CharacterClickListener listener) {
        this.listener = listener;
    }

    public void setItems(List<Character> characterList) {
        this.characterList = characterList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CharacterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_character, parent, false);
        return new CharacterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterViewHolder holder, int position) {
        Character character = characterList.get(position);
        holder.name.setText(character.getName());
        holder.origin.setText(character.getOrigin().getName());
        Glide.with(holder.itemView).load(character.getImage()).into(holder.image);
        holder.itemView.setOnClickListener(view -> listener.onCLick(character));
    }

    @Override
    public int getItemCount() {
        return characterList.size();
    }

    class CharacterViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private TextView origin;
        private ImageView image;

        CharacterViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            origin = itemView.findViewById(R.id.origin);
            origin.setSelected(true);
            image = itemView.findViewById(R.id.image);
        }
    }
}
