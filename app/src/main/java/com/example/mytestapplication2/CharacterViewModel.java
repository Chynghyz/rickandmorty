package com.example.mytestapplication2;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.mytestapplication2.model.Character;
import com.example.mytestapplication2.model.CharacterResponse;
import com.example.mytestapplication2.network.ApiService;
import com.example.mytestapplication2.network.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CharacterViewModel extends ViewModel {
    private ApiService api = new RetrofitClient().getInstance().create(ApiService.class);
    private MutableLiveData<List<Character>> _items = new MutableLiveData<>(new ArrayList<>());
    public LiveData<List<Character>> items = _items;
    private int page = 0;
    private Boolean isLoading = false;
    private Boolean isPagingStopped = false;

    private MutableLiveData<Boolean> _showProgress = new MutableLiveData<>();
    public LiveData<Boolean> showProgress = _showProgress;

    private MutableLiveData<String> _error = new MutableLiveData<>();
    public LiveData<String> error = _error;

    public void getCharacters() {
        if (isLoading || isPagingStopped) return;
        isLoading = true;
        page++;
        _showProgress.postValue(true);
        Call<CharacterResponse> response = api.getAllCharacter(page);
        response.enqueue(new Callback<CharacterResponse>() {
            @Override
            public void onResponse(Call<CharacterResponse> call, Response<CharacterResponse> response) {
                isPagingStopped = response.body().getInfo().getNext() == null;
                _showProgress.postValue(false);
                List<Character> characterList = response.body().getResults();

                List<Character> oldItems = items.getValue();
                oldItems.addAll(characterList);
                _items.postValue(oldItems);
                isLoading = false;
            }

            @Override
            public void onFailure(Call<CharacterResponse> call, Throwable t) {
                _showProgress.postValue(false);
                _error.postValue(t.getMessage());
                page--;
                isLoading = false;
            }
        });
    }

    public void swipeRefresh() {
        page = 0;
        _items.postValue(new ArrayList<>());
        getCharacters();
    }
}
