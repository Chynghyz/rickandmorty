package com.example.mytestapplication2;

import com.example.mytestapplication2.model.Character;

public interface CharacterClickListener {
    void onCLick(Character character);
}
