package com.example.mytestapplication2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.mytestapplication2.detail.CharacterDetailActivity;
import com.example.mytestapplication2.model.Character;
import com.google.android.material.snackbar.Snackbar;

import org.jetbrains.annotations.NotNull;

public class CharacterActivity extends AppCompatActivity {
    private CharacterViewModel viewModel;
    private CharacterAdapter adapter;
    private RecyclerView list;
    private ProgressBar progress;
    private SwipeRefreshLayout swipeRefreshLayout;
    private View container;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character);
        setTitle("Characters");
        viewModel = new ViewModelProvider(this).get(CharacterViewModel.class);
        list = findViewById(R.id.list);
        progress = findViewById(R.id.progress);
        container = findViewById(R.id.container);
        swipeRefreshLayout = findViewById(R.id.swipe_container);
        setAdapter();
        viewModel.getCharacters();
        viewModel.items.observe(this, characterList -> {
            adapter.setItems(characterList);
        });
        viewModel.showProgress.observe(this, show -> {
            if (show) progress.setVisibility(View.VISIBLE);
            else progress.setVisibility(View.GONE);
        });
        viewModel.error.observe(this, error -> {
            snackbar = Snackbar.make(container, error, Snackbar.LENGTH_SHORT);
            if (!snackbar.isShown()) snackbar.show();
        });
    }

    private void setAdapter() {
        adapter = new CharacterAdapter(character -> openCharacter(character));
        LinearLayoutManager lm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        list.setLayoutManager(lm);
        list.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull @NotNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull @NotNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (lm.findLastVisibleItemPosition() == lm.getItemCount() - 1) {
                    viewModel.getCharacters();
                }
            }
        });
        list.setAdapter(adapter);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            viewModel.swipeRefresh();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    private void openCharacter(Character character) {
        Intent intent = new Intent(this, CharacterDetailActivity.class);
        intent.putExtra("character", character);
        startActivity(intent);
    }
}
