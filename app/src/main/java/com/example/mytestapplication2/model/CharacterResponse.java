package com.example.mytestapplication2.model;

import java.util.List;

public class CharacterResponse {
    private Info info;
    private List<Character> results;

    public CharacterResponse(Info info, List<Character> results) {
        this.info = info;
        this.results = results;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Character> getResults() {
        return results;
    }

    public void setResults(List<Character> results) {
        this.results = results;
    }
}
