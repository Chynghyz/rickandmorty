package com.example.mytestapplication2.network;

import com.example.mytestapplication2.model.CharacterResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    @GET("character")
    Call<CharacterResponse> getAllCharacter(@Query("page") int page);
}
